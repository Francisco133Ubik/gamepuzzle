//
//  HomeViewController.swift
//  Game
//
//  Created by Francisco Javier Delgado García on 20/10/20.
//

import UIKit

class HomeViewController: UIViewController {
    private let gradientLayer: CAGradientLayer = {
        let gradientLayer = CAGradientLayer()
        gradientLayer.type = .conic
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0.5)
        return gradientLayer
    }()
    let colors = Colors
    let renderedView = UIView()
    var game = Game()
    let centerPointView = CenterPointView(frame: .zero)
    var displayLink = CADisplayLink()
    var shapeOutlinePath: CGPath?
    var currentTimestamp: CFTimeInterval = .zero
    var currentColorAnimation = UIColor.white.cgColor
    var currentLevel = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupGradient()
        playBackgroundMusic()
    }

    private func setupView(){
        UIApplication.shared.isIdleTimerDisabled = true
        view.addSubview(renderedView)
        renderedView.translatesAutoresizingMaskIntoConstraints = false
        renderedView.backgroundColor = .black
        NSLayoutConstraint.activate([
            renderedView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            renderedView.topAnchor.constraint(equalTo: view.topAnchor),
            renderedView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            renderedView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
        setGame(withLevel: currentLevel)
        setupIndicatorView(center: view.center)
        startDisplayLink()
    }
    
    private func setGame(withLevel level: Int){
        game.initialLevel(level: level, center: CGPoint(x: view.bounds.midX, y: view.bounds.midY))
        for pointView in game.setupControlPoints(){
            view.addSubview(pointView)
            pointView.dragChanged = { [weak self] in
                self?.drawRenderedView(path: self?.game.bezierPath())
            }
            pointView.dragFinished = { [weak self] in
                guard let level = self?.game.currentLevel else {return}
                if level.targetsView.allSatisfy({$0.isInline}){self?.nextLevel()}
            }

        }
        for targetView in game.setupTargetView(){
            view.addSubview(targetView)
        }
        drawRenderedView(path: game.bezierPath())
    }
    
    func drawRenderedView(path: UIBezierPath?){
        guard let path = path else { return }
        let mask = CAShapeLayer()
        mask.fillColor = UIColor.clear.cgColor
        mask.strokeColor = UIColor.white.cgColor
        mask.lineWidth = 5
        mask.path = path.cgPath
        gradientLayer.mask = mask
        centerPointView.layer.borderColor = currentColorAnimation
        centerPointView.layer.removeAllAnimations()
        centerPointView.setAnimationMoveAlongPath(path: path.cgPath,
                                                  timestamp: currentTimestamp)
        centerPointView.setAnimationBorderColor(colors: colors,
                                                timestamp: currentTimestamp)
        shapeOutlinePath = path.cgPath.copy(strokingWithWidth: 10, lineCap: .round, lineJoin: .round, miterLimit: 1)
    }
    
    private func startDisplayLink(){
        displayLink = CADisplayLink(target: self, selector: #selector(handlerDisplayLink(_:)))
        displayLink.add(to: .main, forMode: .common)
    }
    
    @objc func handlerDisplayLink(_ sender: CADisplayLink){
        guard let presentation = centerPointView.layer.presentation() else {
            return
        }
        centerPointView.center = presentation.position
        currentTimestamp = sender.targetTimestamp
        currentColorAnimation = presentation.value(forKey: "borderColor") as! CGColor
        checkStatus(positionCenter: presentation.position)
    }
    
    private func checkStatus(positionCenter: CGPoint){
        guard let outlinePath = shapeOutlinePath,
              let level = game.currentLevel else {
            return
        }
        level.targetsView.forEach({
            $0.isInline = outlinePath.contains($0.center)
            $0.isCorrectPath = level.targetsView.allSatisfy({$0.isInline})
            $0.animate(target: $0.frame.contains(positionCenter))
        })
    }
    
    private func nextLevel(){
        view.isUserInteractionEnabled = false
        currentLevel += 1
        animateNivelCompleted {
            self.setGame(withLevel: self.currentLevel)
            self.gradientLayer.fadeIn(duration: 0.35)
            self.centerPointView.animate([.fadeIn()])
            self.view.isUserInteractionEnabled = true
        }
    }
    
    private func animateNivelCompleted(completion: @escaping()->()){
        successFeedback()
        guard let level = game.currentLevel else {return}
        level.pointsView.forEach { pointView in
            pointView.performAnimations([.fadeOut()]) {
                pointView.removeFromSuperview()
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            level.targetsView.forEach { targetView in
                targetView.performAnimations([.fadeOut()]) {
                    targetView.removeFromSuperview()
                }
            }
            self.centerPointView.layer.removeAllAnimations()
            self.centerPointView.performAnimations([.fadeOut()]) {
                completion()
            }
            self.gradientLayer.fadeOut(duration: 0.35)
        }
    }
    
    private func setupIndicatorView(center: CGPoint){
        centerPointView.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        centerPointView.center = center
        view.addSubview(centerPointView)
    }
    
    private func setupGradient(){
        renderedView.layer.addSublayer(gradientLayer)
        gradientLayer.colors = colors
    }
    
    private func playBackgroundMusic(){
        guard let url = Bundle.main.url(forResource: "background", withExtension: "mp3")  else {
            return
        }
        let player = AVAudioPlayerPool.player(url: url)
        player?.numberOfLoops = -1
        player?.setVolume(0.5, fadeDuration: 0)
        player?.play()
    }
    
    private func successFeedback(){
        let notificationFeedbackGenerator = UINotificationFeedbackGenerator()
        notificationFeedbackGenerator.prepare()
        notificationFeedbackGenerator.notificationOccurred(.success)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        gradientLayer.frame = renderedView.bounds
    }
}
