//
//  Model.swift
//  Game
//
//  Created by Francisco Javier Delgado García on 01/11/20.
//

import UIKit


struct Game {
    var currentLevel: Level? = nil
    
    mutating func initialLevel(level: Int, center: CGPoint) {
        switch level {
        case 1:
            let level = Level(level: 1,
                              pointsView: [ControlPointView(frame: CGRect(x: center.x - CGFloat(132).dp, y: center.y, width: 44, height: 44)),
                                           ControlPointView(frame: CGRect(x: center.x - 22, y: center.y + CGFloat(110).dp, width: 44, height: 44)),
                                           ControlPointView(frame: CGRect(x: center.x + CGFloat(88).dp, y: center.y, width: 44, height: 44))],
                              targetsView: [TargetView(color: UIColor(rgb: 0xD3A800),
                                                       borderColor: UIColor(rgb: 0xA78500),
                                                       frame: CGRect(x: center.x - 11, y: center.y - CGFloat(170).dp, width: 22, height: 22)),
                                            TargetView(color: UIColor(rgb: 0x5CAB85),
                                                       borderColor: UIColor(rgb: 0x3D7359),
                                                       frame: CGRect(x: center.x - CGFloat(101).dp, y: center.y + CGFloat(170).dp, width: 22, height: 22)),
                                            TargetView(color: UIColor(rgb: 0x69A7BE),
                                                       borderColor: UIColor(rgb: 0x528395),
                                                       frame: CGRect(x: center.x + CGFloat(78).dp, y: center.y + CGFloat(170).dp, width: 22, height: 22))])
            self.currentLevel = level
        case 2:
            let level = Level(level: 2,
                              pointsView: [ControlPointView(frame: CGRect(x: center.x - CGFloat(122).dp, y: center.y - CGFloat(100).dp, width: 44, height: 44)),
                                           ControlPointView(frame: CGRect(x: center.x + CGFloat(78).dp, y: center.y - CGFloat(100).dp, width: 44, height: 44)),
                                           ControlPointView(frame: CGRect(x: center.x - CGFloat(122).dp, y: center.y + CGFloat(100).dp, width: 44, height: 44)),
                                           ControlPointView(frame: CGRect(x: center.x + CGFloat(78).dp, y: center.y + CGFloat(100).dp, width: 44, height: 44))],
                              targetsView: [TargetView(color: UIColor(rgb: 0xC7490B),
                                                       borderColor: UIColor(rgb: 0x571800),
                                                       frame: CGRect(x: center.x - CGFloat(90).dp, y: center.y - CGFloat(20).dp, width: 22, height: 22)),
                                            TargetView(color: UIColor(rgb: 0xC7490B),
                                                       borderColor: UIColor(rgb: 0x571800),
                                                       frame: CGRect(x: center.x + CGFloat(80).dp, y: center.y - CGFloat(20).dp, width: 22, height: 22)),
                                            TargetView(color: UIColor(rgb: 0xD58720),
                                                       borderColor: UIColor(rgb: 0x482A00),
                                                       frame: CGRect(x: center.x - 11 , y: center.y - CGFloat(210).dp, width: 22, height: 22)),
                                            TargetView(color: UIColor(rgb: 0xBF2FAB),
                                                       borderColor: UIColor(rgb: 0x621858),
                                                       frame: CGRect(x: center.x - 11 , y: center.y + CGFloat(80).dp, width: 22, height: 22)),
                                            TargetView(color: UIColor(rgb: 0x13E0AC),
                                                       borderColor: UIColor(rgb: 0x084535),
                                                       frame: CGRect(x: center.x - 11, y: center.y - CGFloat(20).dp, width: 22, height: 22))])
            self.currentLevel = level
        default: return
        }
    }
    
    mutating func setupControlPoints() -> [ControlPointView] {
        guard let currentLevel = currentLevel else {
            return [ControlPointView]()
        }
        return currentLevel.pointsView
    }
    
    func bezierPath() -> UIBezierPath?{
        guard let level = currentLevel else {
            return nil
        }
        switch level.level {
        case 1:
            let path = UIBezierPath()
            path.move(to: CGPoint(x: (level.pointsView[2].center.x + level.pointsView[0].center.x) / 2,
                                  y: (level.pointsView[2].center.y + level.pointsView[0].center.y) / 2))
            
            path.addQuadCurve(to: CGPoint(x: (level.pointsView[2].center.x + level.pointsView[1].center.x) / 2,
                                          y: (level.pointsView[1].center.y + level.pointsView[2].center.y) / 2),
                              controlPoint: level.pointsView[2].center)
            
            path.addQuadCurve(to: CGPoint(x: (level.pointsView[0].center.x + level.pointsView[1].center.x) / 2,
                                          y: (level.pointsView[0].center.y + level.pointsView[1].center.y) / 2),
                              controlPoint: level.pointsView[1].center)
            
            path.addQuadCurve(to: CGPoint(x: (level.pointsView[2].center.x + level.pointsView[0].center.x) / 2,
                                          y: (level.pointsView[2].center.y + level.pointsView[0].center.y) / 2),
                              controlPoint: level.pointsView[0].center)
            
            return path
        case 2:
            let path = UIBezierPath()
            path.move(to: CGPoint(x: (level.pointsView[2].center.x + level.pointsView[0].center.x) / 2,
                                  y: (level.pointsView[0].center.y + level.pointsView[2].center.y) / 2))
            path.addQuadCurve(to: CGPoint(x: (level.pointsView[0].center.x + level.pointsView[1].center.x) / 2,
                                          y: (level.pointsView[0].center.y + level.pointsView[1].center.y) / 2),
                              controlPoint: level.pointsView[0].center)
            
            path.addQuadCurve(to: CGPoint(x: (level.pointsView[1].center.x + level.pointsView[3].center.x) / 2,
                                          y: (level.pointsView[1].center.y + level.pointsView[3].center.y) / 2),
                              controlPoint: level.pointsView[1].center)
            
            path.addQuadCurve(to: CGPoint(x: (level.pointsView[2].center.x + level.pointsView[3].center.x) / 2,
                                          y: (level.pointsView[2].center.y + level.pointsView[3].center.y) / 2),
                              controlPoint: level.pointsView[3].center)
            
            path.addQuadCurve(to: CGPoint(x: (level.pointsView[2].center.x + level.pointsView[0].center.x) / 2,
                                          y: (level.pointsView[0].center.y + level.pointsView[2].center.y) / 2),
                              controlPoint: level.pointsView[2].center)
            return path
        default: return nil
        }
    }
    
    func setupTargetView() -> [TargetView]{
        guard let currentLevel = currentLevel else {
            return [TargetView]()
        }
        return currentLevel.targetsView
    }
    
}


struct Level {
    let level: Int
    let pointsView: [ControlPointView]
    let targetsView: [TargetView]
}


let Colors = [UIColor(rgb: 0xDE0DA9).cgColor,
              UIColor(rgb: 0x00F7BB).cgColor,
              UIColor(rgb: 0xBE1111).cgColor,
              UIColor(rgb: 0xD3A400).cgColor,
              UIColor(rgb: 0xDE0DA9).cgColor]
