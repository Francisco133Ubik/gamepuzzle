//
//  IndicatorView.swift
//  Game
//
//  Created by Francisco Javier Delgado García on 22/10/20.
//

import UIKit

class TargetView: UIView{
    let centerView = UIView(frame: .zero)
    let color: UIColor
    let borderColor: UIColor
    var animationInProgress = false
    
    var isInline = false{didSet{
        if isInline == false{
            animationInProgress = false
            animate([.color(.clear)])
        }
    }}
    
    var isCorrectPath = false
    
    init(color: UIColor, borderColor: UIColor, frame: CGRect) {
        self.color = color
        self.borderColor = borderColor
        super.init(frame: frame)
        setupView()
    }
    
    private func setupView(){
        tag = 1
        backgroundColor = .clear
        layer.borderWidth = 4
        layer.borderColor = borderColor.cgColor
        addSubview(centerView)
        centerView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            centerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            centerView.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            centerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
            centerView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0)
        ])
        centerView.backgroundColor = .clear
        centerView.clipsToBounds = true
        centerView.layer.masksToBounds = true
        centerView.layer.borderWidth = 4
        centerView.layer.borderColor = borderColor.cgColor
        transform = CGAffineTransform(rotationAngle: 45 * .pi/180)
    }
    
    public func animate(target: Bool){
        guard !animationInProgress else { return }
        if target{
            guard isInline else {return}
            animationInProgress = true
            performAnimationsInParallel([
                .color(isCorrectPath ? .white : color),
                .applyTransform(CGAffineTransform(scaleBy: 1.15).rotated(by: 225 * .pi/180)),

            ]) {
                self.animationInProgress = false
                self.animate([.applyTransform(CGAffineTransform(scaleBy: 1).rotated(by:  45 * .pi/180),
                                              delay: 0.1)])
            }
            if isCorrectPath{
                centerView.animateBorderColor(toColor: .white, duration: 0.35)
                animateBorderColor(toColor: .white, duration: 0.35)
            }
            playSoundOn()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        centerView.layer.cornerRadius = centerView.frame.height / 2
    }
    
    override var intrinsicContentSize: CGSize{
        return CGSize(width: 22, height: 22)
    }
    
    private func playSoundOn(){
        guard let url = Bundle.main.url(forResource: "soundIn", withExtension: "mp3")  else {
            return
        }
        let player = AVAudioPlayerPool.player(url: url)
        player?.setVolume(0.6, fadeDuration: 0)
        player?.play()
    }
}
