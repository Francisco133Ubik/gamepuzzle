//
//  ControlPointView.swift
//  Game
//
//  Created by Francisco Javier Delgado García on 22/10/20.
//

import UIKit

class ControlPointView: UIView {
    var backgroundView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
    var centerView = UIView()
    var dragChanged: (() -> Void)?
    var dragFinished: (() -> Void)?
    var touchStartPos = CGPoint.zero
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView(){
        backgroundColor = .clear
        clipsToBounds = true
        layer.masksToBounds = true
        
        addSubview(backgroundView)
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            backgroundView.leadingAnchor.constraint(equalTo: leadingAnchor),
            backgroundView.trailingAnchor.constraint(equalTo: trailingAnchor),
            backgroundView.topAnchor.constraint(equalTo: topAnchor),
            backgroundView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        backgroundView.alpha = 0
        
        addSubview(centerView)
        centerView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            centerView.centerYAnchor.constraint(equalTo: centerYAnchor),
            centerView.centerXAnchor.constraint(equalTo: centerXAnchor),
            centerView.heightAnchor.constraint(equalToConstant: 22),
            centerView.widthAnchor.constraint(equalToConstant: 22),
        ])
        centerView.backgroundColor = .white
        centerView.clipsToBounds = true
        centerView.layer.masksToBounds = true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        touchStartPos = touch.location(in: self)
        touchStartPos.x -= frame.width / 2
        touchStartPos.y -= frame.height / 2
        
        
        animate([
            .applyTransform(CGAffineTransform(scaleBy: 1.2))
        ])
        backgroundView.performAnimations([
            .fadeIn()
        ]) {
            self.backgroundView.animate([.fadeOut(duration: 0.8)])
        }
        
        generateHapticFeedback()
        playSoundFeedback()
        superview?.bringSubviewToFront(self)
        
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first,
              let superview = superview else { return }
        let point = touch.location(in: superview)
        let restrictByPointX: CGFloat = 30.0
        let restrictByPointY: CGFloat = 10.0
        let offsetBottom = superview.safeAreaInsets.top + superview.safeAreaInsets.bottom
        let superBounds = CGRect(x: superview.bounds.origin.x + restrictByPointX,
                                 y: superview.safeAreaInsets.top + restrictByPointY,
                                 width: superview.bounds.size.width - 2 * restrictByPointX,
                                 height: superview.bounds.size.height - 2 * restrictByPointY - offsetBottom)
        
        if (superBounds.contains(point)) {
            center = CGPoint(x: point.x - touchStartPos.x,
                             y: point.y - touchStartPos.y)
            dragChanged?()
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        animate([.applyTransform(.identity)])
        backgroundView.animate([.fadeOut()])
        dragFinished?()
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        touchesEnded(touches, with: event)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        centerView.layer.cornerRadius = 11
        layer.cornerRadius = 22
    }
    
    override var intrinsicContentSize: CGSize{
        return CGSize(width: 44, height: 44)
    }
    
    private func generateHapticFeedback(){
        let selectionFeedbackGenerator = UISelectionFeedbackGenerator()
        selectionFeedbackGenerator.selectionChanged()
    }
    
    private func playSoundFeedback(){
        guard let url = Bundle.main.url(forResource: "soundOff", withExtension: "mp3")  else {
            return
        }
        let player = AVAudioPlayerPool.player(url: url)
        player?.setVolume(0.3, fadeDuration: 0)
        player?.play()
    }
}
