//
//  AudioPlayer.swift
//  Game
//
//  Created by Francisco Javier Delgado García on 01/11/20.
//

import Foundation
import AVFoundation

private var players : [AVAudioPlayer] = []

class AVAudioPlayerPool: NSObject {

    class func player(url : URL) -> AVAudioPlayer? {

        let availablePlayers = players.filter { (player) -> Bool in
            return player.isPlaying == false && player.url == url
        }

        if let playerToUse = availablePlayers.first {
            return playerToUse
        }

        do {
            let newPlayer = try AVAudioPlayer(contentsOf: url)
            players.append(newPlayer)
            return newPlayer
        } catch {
            return nil
        }
    }

}
