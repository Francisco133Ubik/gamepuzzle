//
//  Extensions.swift
//  Game
//
//  Created by Francisco Javier Delgado García on 30/10/20.
//

import UIKit

extension CGFloat {
    /**
     The relative dimension to the corresponding screen size.
     **Warning** Only works with size references from @1x mockups.
     */
    var dp: CGFloat {
        if UIDevice.current.userInterfaceIdiom == .phone {
            //Design base in iPhone 8 Plus
            return (self / 414) * UIScreen.main.bounds.width
        }else{
            #if targetEnvironment(macCatalyst)
            //code to run on macOS
            return ((self / 1024) * UIScreen.main.bounds.height ) * UIScreen.main.scale
            #else
            //code to run on iOS
            //Desing base ipad Pro (12.9-inch)
            return (self / 1024) * UIScreen.main.bounds.height
            #endif
        }
    }
    
}


extension UIColor {
   convenience init(red: Int, green: Int, blue: Int) {
       assert(red >= 0 && red <= 255, "Invalid red component")
       assert(green >= 0 && green <= 255, "Invalid green component")
       assert(blue >= 0 && blue <= 255, "Invalid blue component")
       self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
   }

   convenience init(rgb: Int) {
       self.init(
           red: (rgb >> 16) & 0xFF,
           green: (rgb >> 8) & 0xFF,
           blue: rgb & 0xFF
       )
   }
}

extension UIView {
    func animateBorderColor(toColor: UIColor, duration: Double) {
        let animation:CABasicAnimation = CABasicAnimation(keyPath: "borderColor")
        animation.fromValue = layer.borderColor
        animation.toValue = toColor.cgColor
        animation.duration = duration
        layer.add(animation, forKey: "borderColor")
        layer.borderColor = toColor.cgColor
    }
    
    func setAnimationMoveAlongPath(path: CGPath, timestamp: CFTimeInterval){
        let moveAlongPath = CAKeyframeAnimation(keyPath: "position")
        moveAlongPath.path = path
        moveAlongPath.duration = 5
        moveAlongPath.repeatCount = .infinity
        moveAlongPath.calculationMode = .cubicPaced
        moveAlongPath.timingFunctions = [CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)]
        moveAlongPath.isRemovedOnCompletion = false
        moveAlongPath.fillMode = .forwards
        moveAlongPath.timeOffset = timestamp
        layer.add(moveAlongPath, forKey: "path")
    }
    
    func setAnimationBorderColor(colors: [CGColor], timestamp: CFTimeInterval){
        let colorBorder = CAKeyframeAnimation(keyPath: "borderColor")
        colorBorder.values = colors
        colorBorder.repeatCount = .infinity
        colorBorder.isRemovedOnCompletion = false
        colorBorder.fillMode = .forwards
        colorBorder.timeOffset = timestamp
        colorBorder.duration = 15
        layer.add(colorBorder, forKey: "borderColor")
    }
}


extension CALayer {
    func fadeOut(duration: TimeInterval) {
        let fadeOut = CABasicAnimation(keyPath: "opacity")
        fadeOut.fromValue = 1
        fadeOut.toValue = 0
        fadeOut.duration = duration
        fadeOut.fillMode = .forwards
        fadeOut.isRemovedOnCompletion = false
        add(fadeOut, forKey: "fadeOut")
        opacity = 0
    }
    
    func fadeIn(duration: TimeInterval) {
        let fadeIn = CABasicAnimation(keyPath: "opacity")
        fadeIn.fromValue = 0
        fadeIn.toValue = 1
        fadeIn.duration = duration
        fadeIn.fillMode = .forwards
        fadeIn.isRemovedOnCompletion = false
        add(fadeIn, forKey: "fadeIn")
        opacity = 1
    }
}
