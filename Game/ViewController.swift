//
//  HomeViewController.swift
//  Game
//
//  Created by Francisco Javier Delgado García on 20/10/20.
//

import UIKit

class HomeViewController: UIViewController {
    private let gradientLayer: CAGradientLayer = {
           let gradientLayer = CAGradientLayer()
           gradientLayer.type = .conic
           gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.5)
           gradientLayer.endPoint = CGPoint(x: 1, y: 0.5)
           return gradientLayer
       }()
    var lineWidth:  CGFloat = 3
    
    let renderedView = UIImageView()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView(){
        view.addSubview(renderedView)
        renderedView.translatesAutoresizingMaskIntoConstraints = false
        renderedView.backgroundColor = .black
        renderedView.layer.addSublayer(gradientLayer)
        gradientLayer.colors = [UIColor(rgb: 0xDE0DA9).cgColor,
                                UIColor(rgb: 0x00F7BB).cgColor,
                                UIColor(rgb: 0xBE1111).cgColor,
                                UIColor(rgb: 0xD3A400).cgColor]
        NSLayoutConstraint.activate([
            renderedView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            renderedView.topAnchor.constraint(equalTo: view.topAnchor),
            renderedView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            renderedView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
        drawRenderedView()
    }
    
    func drawRenderedView(){
//        let renderer = UIGraphicsImageRenderer(bounds: view.bounds)
//        renderedView.image = renderer.image { ctx in
//            ctx.cgContext.move(to: CGPoint(x: 100, y: 200))
//            ctx.cgContext.addQuadCurve(to: CGPoint(x: 300, y: 200),
//                                       control: CGPoint(x: 200, y: 300))
//            ctx.cgContext.setStrokeColor(UIColor.red.cgColor)
        //            ctx.cgContext.strokePath()
        //        }
        
        let center = CGPoint(x: view.bounds.midX, y: view.bounds.midY)
        let radius = (min(view.bounds.width, view.bounds.height) - lineWidth) / 2
        let path = UIBezierPath(arcCenter: center,
                                radius: radius,
                                startAngle: 0,
                                endAngle: 2 * .pi,
                                clockwise: true)
        let mask = CAShapeLayer()
        mask.fillColor = UIColor.clear.cgColor
        mask.strokeColor = UIColor.white.cgColor
        mask.lineWidth = lineWidth
        mask.path = path.cgPath
        gradientLayer.mask = mask
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        gradientLayer.frame = renderedView.bounds
    }
}


extension UIColor {
   convenience init(red: Int, green: Int, blue: Int) {
       assert(red >= 0 && red <= 255, "Invalid red component")
       assert(green >= 0 && green <= 255, "Invalid green component")
       assert(blue >= 0 && blue <= 255, "Invalid blue component")

       self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
   }

   convenience init(rgb: Int) {
       self.init(
           red: (rgb >> 16) & 0xFF,
           green: (rgb >> 8) & 0xFF,
           blue: rgb & 0xFF
       )
   }
}
